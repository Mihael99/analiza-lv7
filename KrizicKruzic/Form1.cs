﻿/*Napravite  igru  križić-kružić  (iks-oks) korištenjem  znanja  stečenih  na  ovoj laboratorijskoj vježbi. 
 Omogućiti pokretanje igre, unos imena dvaju igrača, ispis koji igrač je trenutno na potezu, 
 igranje igre s iscrtavanjem križića i kružića na odgovarajućim  mjestima  te  ispis  dijaloga  s  porukom  o  pobjedi,
 odnosno neriješenom rezultatu kao i praćenje ukupnog rezultata. */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KrizicKruzic
{
    public partial class Form1 : Form
    {
        int prvi;
        int drugi;
        int counter = 1;
        int temp = 1;
        int click1 = 0;
        int click2 = 0;
        int click3 = 0;
        int click4 = 0;
        int click5 = 0;
        int click6 = 0;
        int click7 = 0;
        int click8 = 0;
        int click9 = 0;

        public Form1()
        {
            InitializeComponent();
            Red();
            lb3.Hide();
            lb4.Hide();
            lb5.Hide();
            lb6.Hide();
            button1.Hide();
            button2.Hide();
            button3.Hide();
            button4.Hide();
            button5.Hide();
            button6.Hide();
            button7.Hide();
            button8.Hide();
            button9.Hide();
            bt4.Hide();
        }

        //Tipka za pokrenut igru
        private void bt1_Click(object sender, EventArgs e)
        {
            if (tB1.Text.Any() && tB2.Text.Any())
            {
                button1.Show();
                button2.Show();
                button3.Show();
                button4.Show();
                button5.Show();
                button6.Show();
                button7.Show();
                button8.Show();
                button9.Show();
                bt4.Show();
                Red();
            }
        }

        //Koji igrač je na redu za stavit X ili O
        public void Red()
        {
            if(counter%2 != 0)
            {
                lb4.Hide();
                lb3.Text = tB1.Text.ToString() + " na redu!";
                lb3.Show();
            }
            else
            {
                lb3.Hide();
                lb4.Text = tB2.Text.ToString() + " na redu!";
                lb4.Show();
            }
        }

        //Provjera pobjednika ili nerjesenog rezultata
        public void Rezultat()
        {
            if ((button1.Text == "X") && (button2.Text == "X") && (button3.Text == "X") || (button1.Text == "X") && (button5.Text == "X") && (button9.Text == "X") || (button1.Text == "X") && (button4.Text == "X") && (button7.Text == "X")) 
            {
                MessageBox.Show("Pobjednik je " + tB1.Text.ToString());
                prvi++;
                lb5.Text = prvi.ToString();
                lb5.Show();
                Clear();
            }
            else if ((button4.Text == "X") && (button5.Text == "X") && (button6.Text == "X"))
            {
                MessageBox.Show("Pobjednik je " + tB1.Text.ToString());
                prvi++;
                lb5.Text = prvi.ToString();
                lb5.Show();
                Clear();
            }
            else if ((button7.Text == "X") && (button8.Text == "X") && (button9.Text == "X") || (button7.Text == "X") && (button5.Text == "X") && (button3.Text == "X") || (button7.Text == "X") && (button8.Text == "X") && (button9.Text == "X"))
            {
                MessageBox.Show("Pobjednik je " + tB1.Text.ToString());
                prvi++;
                lb5.Text = prvi.ToString();
                lb5.Show();
                Clear();
            }
            else if ((button2.Text == "X") && (button5.Text == "X") && (button8.Text == "X") || (button3.Text == "X") && (button6.Text == "X") && (button9.Text == "X"))
            {
                MessageBox.Show("Pobjednik je " + tB1.Text.ToString());
                prvi++;
                lb5.Text = prvi.ToString();
                lb5.Show();
                Clear();
            }
            else if ((button1.Text == "O") && (button2.Text == "O") && (button3.Text == "O") || (button1.Text == "O") && (button5.Text == "O") && (button9.Text == "O") || (button1.Text == "O") && (button4.Text == "O") && (button7.Text == "O"))
            {
                MessageBox.Show("Pobjednik je " + tB2.Text.ToString());
                drugi++;
                lb6.Text = drugi.ToString();
                lb6.Show();
                Clear();
            }
            else if ((button4.Text == "O") && (button5.Text == "O") && (button6.Text == "O"))
            {
                MessageBox.Show("Pobjednik je " + tB2.Text.ToString());
                drugi++;
                lb6.Text = drugi.ToString();
                lb6.Show();
                Clear();
            }
            else if ((button7.Text == "O") && (button8.Text == "O") && (button9.Text == "O") || (button7.Text == "O") && (button5.Text == "O") && (button3.Text == "O") || (button7.Text == "O") && (button8.Text == "O") && (button9.Text == "O"))
            {
                MessageBox.Show("Pobjednik je " + tB2.Text.ToString());
                drugi++;
                lb6.Text = drugi.ToString();
                lb6.Show();
                Clear();
            }
            else if ((button2.Text == "O") && (button5.Text == "O") && (button8.Text == "O") || (button3.Text == "O") && (button6.Text == "O") && (button9.Text == "O"))
            {
                MessageBox.Show("Pobjednik je " + tB2.Text.ToString());
                drugi++;
                lb6.Text = drugi.ToString();
                lb6.Show();
                Clear();
            }
            else if (counter == temp + 9)
            {
                MessageBox.Show("Nerjeseno je!");
                Clear();
            }
        }

        //Svih 9 kocki za upisi X i O
        private void button1_Click(object sender, EventArgs e)
        {
            if (click1 == 0)
            {
                if (counter % 2 != 0)
                {
                    button1.Text = "X";
                    counter++;
                    click1++;
                    Red();
                    Rezultat();
                }
                else
                {
                    button1.Text = "O";
                    counter++;
                    click1++;
                    Red();
                    Rezultat();
                }
            }     
        }
        private void button2_Click(object sender, EventArgs e)
        {
          
            if (click2 == 0)
            {
                if (counter % 2 != 0)
                {
                    button2.Text = "X";
                    counter++;
                    click2++;
                    Red();
                    Rezultat();
                }
                else
                {
                    button2.Text = "O";
                    counter++;
                    click2++;
                    Red();
                    Rezultat();
                }
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
           
            if (click3 == 0)
            {
                if (counter % 2 != 0)
                {
                    button3.Text = "X";
                    counter++;
                    click3++;
                    Red();
                    Rezultat();
                }
                else
                {
                    button3.Text = "O";
                    counter++;
                    click3++;
                    Red();
                    Rezultat();
                }
            }
        }
        private void button4_Click(object sender, EventArgs e)
        {
            
            if (click4 == 0)
            {
                if (counter % 2 != 0)
                {
                    button4.Text = "X";
                    counter++;
                    click4++;
                    Red();
                    Rezultat();
                }
                else
                {
                    button4.Text = "O";
                    counter++;
                    click4++;
                    Red();
                    Rezultat();
                }
            }
        }
        private void button5_Click(object sender, EventArgs e)
        {
           
            if (click5 == 0)
            {
                if (counter % 2 != 0)
                {
                    button5.Text = "X";
                    counter++;
                    click5++;
                    Red();
                    Rezultat();
                }
                else
                {
                    button5.Text = "O";
                    counter++;
                    click5++;
                    Red();
                    Rezultat();
                }
            }
        }
        private void button6_Click(object sender, EventArgs e)
        {
          
            if (click6 == 0)
            {
                if (counter % 2 != 0)
                {
                    button6.Text = "X";
                    counter++;
                    click6++;
                    Red();
                    Rezultat();
                }
                else
                {
                    button6.Text = "O";
                    counter++;
                    click6++;
                    Red();
                    Rezultat();
                }
            }
        }
        private void button7_Click(object sender, EventArgs e)
        {
            
            if (click7 == 0)
            {
                if (counter % 2 != 0)
                {
                    button7.Text = "X";
                    counter++;
                    click7++;
                    Red();
                    Rezultat();
                }
                else
                {
                    button7.Text = "O";
                    counter++;
                    click7++;
                    Red();
                    Rezultat();
                }
            }
        }
        private void button8_Click(object sender, EventArgs e)
        {
            
            if (click8 == 0)
            {
                if (counter % 2 != 0)
                {
                    button8.Text = "X";
                    counter++;
                    click8++;
                    Red();
                    Rezultat();
                }
                else
                {
                    button8.Text = "O";
                    counter++;
                    click8++;
                    Red();
                    Rezultat();
                }
            }
        }
        private void button9_Click(object sender, EventArgs e)
        {
            
            if (click9 == 0)
            {
                if (counter % 2 != 0)
                {
                    button9.Text = "X";
                    counter++;
                    click9++;
                    Red();
                    Rezultat();
                }
                else
                {
                    button9.Text = "O";
                    counter++;
                    click9++;
                    Red();
                    Rezultat();
                }
            }
        }

        //Restart(Ako zelimo upisat nova imena) i exit iz aplikacije
        private void bt2_Click(object sender, EventArgs e)
        {
            Application.Restart();
            Environment.Exit(0);
        }

        private void bt3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //Nova igra tipka ako zelimo tijekom igre krenuti ispocetka, i clear da krene ponovno nakon završene igre
        private void bt4_Click(object sender, EventArgs e)
        {
            button1.Text = " ";
            button2.Text = " ";
            button3.Text = " ";
            button4.Text = " ";
            button5.Text = " ";
            button6.Text = " ";
            button7.Text = " ";
            button8.Text = " ";
            button9.Text = " ";
            if (temp == 1)
            {
                counter = 2;
                temp = counter;
            }
            else if (temp == 2)
            {
                counter = 1;
                temp = counter;
            }
            click1 = 0;
            click2 = 0;
            click3 = 0;
            click4 = 0;
            click5 = 0;
            click6 = 0;
            click7 = 0;
            click8 = 0;
            click9 = 0;
            Red();
        }

        public void Clear()
        {
            button1.Text = " ";
            button2.Text = " ";
            button3.Text = " ";
            button4.Text = " ";
            button5.Text = " ";
            button6.Text = " ";
            button7.Text = " ";
            button8.Text = " ";
            button9.Text = " ";
            if(temp == 1)
            {
                counter = 2;
                temp = counter;
            }
            else if (temp == 2)
            {
                counter = 1;
                temp = counter;
            }
            click1 = 0;
            click2 = 0;
            click3 = 0;
            click4 = 0;
            click5 = 0;
            click6 = 0;
            click7 = 0;
            click8 = 0;
            click9 = 0;
            Red();
        }
    }
}
